# Catalog

Tracked in this repository are changes to the [RDMO catalog](https://github.com/rdmorganiser/rdmo-catalog/tree/master/rdmorganiser) done for the GRO.plan instance. They are exported from the web interface of the production [GRO.plan](https://plan.goettingen-research-online.de) system.

# Branches

The main branch should reflect the catalog on the [production server](https://plan.goettingen-research-online.de). The develop branch should reflect changes which should be added to the production system, and may be in sync with the [test system](https://test.plan.goettingen-research-online.de)

# How to use

When you make changes to the gro plan catalog, please export as XML file and store it in the appropriate subfolder.
To do so chosse "Export > XML" from the side bar in the catalog management view of RDMO. Please use the "Save Link as" context
menu, as e.g. Firefox modifies XML slightly (&qout; is written as ") if you open with Firefox and save afterwards.

If you choose to update this git repo from your web browser you may enter the choosen file in the gitlab interface and use
the "Replace" Button.

# Overview of imported catalogues
https://gitlab.gwdg.de/era-public/plan/-/wikis/Overview-of-Questionnaires

